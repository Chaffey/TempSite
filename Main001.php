<!--///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
										PHP
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////-->
<?php
session_start();
error_reporting(0);
//*********************************DB Login Credentials*************************************
$username = "root";
$password = "35264";
$hostname = "localhost";
$dbname = "mydb";

//...Variables...
$_SESSION['counter'];
$_SESSION['status'];
$_SESSION['page'];
$con = null;

//..........connection to the database..........
function dbConnect() {
	global $hostname, $password, $username, $con;
	$con = $dbhandle = mysqli_connect($hostname, $username, $password);
	if (!$dbhandle) {
		throw new Exception('<font color="white">Connection Failed!!</font>');
	}
}
try{
	dbConnect();
	echo '<font color="white">Connection Successful!!</font></br>';
}
catch (Exception $e)
{
	echo '<font color="white">ERROR...</font> ', $e->getMessage();
	echo "</br>";
}

//..........select a database to work with..........
function dbSelect() {
	global $con, $dbname;
	$selected = mysqli_select_db($con, $dbname);
	if(!$selected) {
		throw new Exception('<font color="white">Could not select '.$dbname.'</font>');
	}
}
try{
	dbSelect();
	echo '<font color="white">'.$dbname.' Selected Successfully</font></br>';
}
catch(Exception $e1){
	echo '<font color="white">ERROR...</font> ', $e1->getMessage();
}

?>






<!--///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
										HTML
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////-->
<html>
<head>

	<meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<!--====================================Style Sheets================================-->
	<link rel="stylesheet" href="Main001.css"/>
	<link rel="stylesheet" type="text/css" href="Cagliostro-Regular.ttf" />

	<title>
		Majestic Hair
	</title>

</head>

<!--======================================================================================-->
<!--================================= Body ================================================-->
<!--======================================================================================-->
<body id="homeTop">

<!--================================= Header ================================================-->
	<header id="b">
		<center>
			<table width="100%">
				<tr>
					<td width="100%" align="center">
						<div id="a" class="headh1">
							MAJESTIC HAIR CREATIONS
						</div>
					</td>
				</tr>
				<tr class="menu">
					<td align="center">
						<nav>
							<a href="#" id="menu-icon"></a>
							<ul>
								<li>
									<a href="#" id="c">HOME</a>
								</li>
								<li id="c">
									<a href="#" id="d">SERVICES</a>
								</li>
								<li id="g">
									<a href="#" id="h">PRODUCTS</a>
								</li>
								<li id="c">
									<a href="#" id="e">ABOUT US</a>
								</li>
								<li id="c">
									<a href="#" id="f">CONTACT</a>
								</li>
								<li id="c">
									<a href="#" id="g">BOOK ONLINE</a>
								</li>
								<li id="c">
									<a href="#" id="h">SHOP</a>
								</li>
							</ul>
						</nav>
					</td>
				</tr>
			</table>
		</center>
	</header>
<!--================================= ScrollElement ================================================-->

<img src="images/up.png" class="scrltop" id="totop" />

<!--================================= DB Status ================================================-->
<?php
/*


*/

?>
<!--================================= Content ================================================-->
	<div id="text" class="content">
		<div class="center-block">
			<!--================================= Main ================================================-->
			<div class="main">
				<font id="huge">
					MAJESTIC
					</br>
					HAIR
					</br>
					CREATIONS
				</font>
				<font id="med">
					</br>
					POTCHEFSTROOM
				</font>
			</div>


		</div>
		<!--================================= Second ================================================-->
		<div class="second" id="two">
			<div>
				<center>
					<table width=100%>
						<tr>
							<td width=100% align=center class="padstop">
								<font id="medPlus">
									SERVICES
									</br>
									<img src="images/decorative-line-clipart-dTr57eXT9.png" class="secondSpace" />
								</font>
							</td>
						</tr>
						<!-------------------- Images ---------------------->
						<tr class="line">
							<table align=center width=70% class="TableOnePad padsbottom">
								<td width=33% align=center class="chng">
									<a href=""><img class="tabimg" src="images/barber-scissors-xxl.png" onmouseover="this.src='images/barber-scissors-xxl (1).png'" onmouseout="this.src='images/barber-scissors-xxl.png'" onclick="this.src='images/barber-scissors-xxl.png'" alt="Hairstyling" /></a>
									</br></br></br></br></br>
									<font id="med" class="chng">HAIRSTYLES</font>
								</td>
								<td width=33% align=center>
									<a href=""><img class="tabimg" src="images/color-palette-white.png" onmouseover="this.src='images/11-512.png'" onmouseout="this.src='images/color-palette-white.png'" alt="HairColors" /></a>
									</br></br></br></br></br>
									<font id="med">HAIRCOLOR</font>
								</td>
								<td width=33% align=center>
									<a href=""><img class="tabimg" src="images/add-button-white-hi.png" onmouseover="this.src='images/plus-5-xxl.png'" onmouseout="this.src='images/add-button-white-hi.png'" alt="HairCare"/></a>
									</br></br></br></br></br>
									<font id="med">HAIRCARE</font>
								</td>
							</table>
						</tr>
						
					</table>
				</center>
			</div>
		</div>

		
		<!--================================= Third ================================================-->
		<div class="third" id="three">
			<center>
				<table width=100%>
					<tr>
						<td align=center width=70%>
						<div class="container">
							<font id="prod">Products</font>
							</br>
							<img src="images/Decorative-Line-Black-Free-Download-PNG.png" style="max-width:50%;"/>
							<ul class="grid-nav">
								<li><a href="#" data-id="two-columns" class="active">1</a></li>
								<li><a href="#" data-id="three-columns">2</a></li>
								<li><a href="#" data-id="four-columns">3</a></li>
							</ul>

							<hr />

							<div id="two-columns" class="grid-container" style="display:block;">
								<ul class="rig columns-2">
									<li>
										<a href="#popup1"><img src="images/Products/001.jpg" /></a>
										<h3>Styling</h3>
										<p>We can do all popular styles.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/002.jpg" /></a>
										<h3>Moisturiser</h3>
										<p>Moisturise your hair for that ultra shine.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/003.jpg" /></a>
										<h3>Conditioner</h3>
										<p>Take care of your hair with the best conditioning products to date.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/004.jpg" /></a>
										<h3>Wax</h3>
										<p>For that natural ragged look.</p>
									</li>
								</ul>
							</div>
							<!--/#two-columns-->

							<div id="three-columns" class="grid-container">
								<ul class="rig columns-3">
									<li>
										<a href="#popup1"><img src="images/Products/005.jpg" /></a>
										<h3>Precision Cut</h3>
										<p>All those extremely difficult styles.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/006.jpg" /></a>
										<h3>Specialist Tools</h3>
										<p>Tools used by professionals for specialising.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/007.jpg" /></a>
										<h3>Clippers</h3>
										<p>Easy to use, easy to clean. Save yourself a lot of time.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/008.jpg" /></a>
										<h3>Dryers</h3>
										<p>Industrial, everyday use and portable dryers.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/009.jpg" /></a>
										<h3>Scissor Sets</h3>
										<p>Get any and every type of scissor you need.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/010.jpg" /></a>
										<h3>Starter Kits</h3>
										<p>For those aspiring hair-stylists.</p>
									</li>
								</ul>
							</div>
							<!--/#three-columns-->

							<div id="four-columns" class="grid-container">
								<ul class="rig columns-4">
									<li>
										<a href="#popup1"><img src="images/Products/011.jpg" /></a>
										<h3>Styling kit</h3>
										<p>All the tools you need to style your hair in any way you can think of.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/012.jpg" /></a>
										<h3>Hair Spray</h3>
										<p>For those quick touch-ups and ultra strong hold.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/013.jpg" /></a>
										<h3>Shaving Cream</h3>
										<p>Feel like a real man with these assorted creams.</p>
									</li>
									<li>
										<a href="#popup1"><img src="images/Products/014.jpg" /></a>
										<h3>Hair Dye</h3>
										<p>Only the best coloring products for the perfect result.</p>
									</li>
								</ul>
							</div>
							<!--/#four-columns-->

							<hr />

						</div>
						
						</td>
					</tr>
				</table>
			</center>
		</div>

		<!--================================= Forth ================================================-->
		<div class="forth" id="four">
			<div class="about">
					<center>
						<table  class="tableAbout" align="center">
							<tr>
								<td class="padsMin" width="100%" align="center">
									<font id="medPlus">ABOUT US</font>
								</td>
							</tr>
							<tr class="padsMin">
								<td align="center">
									<p><font id="med">ALPHA Q</font></p>
									</br>
									<font id="small">
										<p>MEMBERS:</br></br>Fred</br>Altus</br>Edwin</br>Wynand</br>Stian</br>Stephan</br>Simeon</br></p>
									</font>
								</td>
							</tr>
						</table>
					</center>
			</div>
		</div>

		<!--================================= Fifth ================================================-->
		<div class="fifth" id="five">
			<div class="about">
				<center>
					<table class="tableAbout" align="center">
						<tr>
							<td class="padsMin" width="100%" align="center">
								<font id="medPlus">FIND US</font>
							</td>
						</tr>

						<tr>
							<td>
								<ul>
									<li>
										<font  id="small">
											70 Steve Biko Street Potchefstroom, 2520
										</font>
										</br></br>
										<font id="small">
											</br>mhcpotchefstroom @gmail.com
										</font>
										<font id="small">
											</br></br>Tel: 123-456-7890
											</br></br></br></br>
											OPENING HOURS:
											</br>
											<p>
											<table width="100%" align="center">
												<tr>
													<td>
														<font id="small">Mon - Fri:</font>
													</td>
													<td>
														<font id="small">7am - 10pm</font>
													</td>
												</tr>
												<tr>
													<td>
														<font id="small">Saturday:</font>
													</td>
													<td>
														<font id="small">8am - 10pm</font>
													</td>
												</tr>
												<tr>
													<td>
														<font id="small">​Sunday:</font>
													</td>
													<td>
														<font id="small">8am - 11pm</font>
													</td>
												</tr>
											</table>
											</p>
										</font>
									</li>
									<li>
										</br>
										<font id="small">
											Have a question or complaint?</br>Send us an email and we will contact you as soon as possible.
										</font>
										<form action="MailStatus.php" method="post" name="contactform">
											</br></br>
											<input class="stdIn" id="namefield" name="name" type="text" placeholder="Name" />
											</br></br>
											<input class="stdIn" id="emailfield" name="email" type="text" placeholder="Email" />
											</br></br>
											<input class="stdIn" id="subjectfield" name="subject" type="text" placeholder="Subject" />
											</br></br>
											<textarea class="stdIn" id="messagefield" name="message" cols="40" rows="5" placeholder="Message" ></textarea>
											</br></br>
											<input type="submit" class="sendBut" name="SendBut" value="Send" onclick="getInput()">
										</form>
									</li>
								</ul>
							</td>
						</tr>



						</table>
						</td>
						</tr>
					</table>
				</center>
			</div>
		</div>
		<p id="demo"></p>
	</div>

<?php
//.................................... Send email to contact us ........................................

	if(isset($_POST['restart'])){
		if($_SESSION['counter'] > 5)
		session_unset();
		session_destroy();
	}




	if(isset($_POST['SendBut'])){
		$_SESSION['page'] ++;
		$tmp01 = $_SESSION['page'];

		if($_SESSION['page'] <= 1){
			$to = "chaffey007@gmail.com";
			$from = $_POST['email'];
			$first_name = $_POST['name'];
			$subject = $_POST['subject'];
			$message = $first_name . " " . "wrote the following:" . "\n\n" . $_POST['message'];
			$headers = "From: " . $from;

			function mailform() {
				global $to, $subject, $message, $headers;
				mail($to,$subject,$message,$headers);
				if(!mail) {
					throw new Exception('<font color="white">Sending Failed</font>');
				}
			}
			try{
				mailform();
				echo "<font color='white'>$tmp01</font>";
				echo '</br>';
				echo '<font color="white">Email Sent Successfully!</br></br>Thank you, '.$first_name.'. We will contact you soon. </font></br>';

			}
			catch(Exception $e1){
				echo '<font color="white">ERROR...</font> ', $e1->getMessage();
			}

			
		}
	}





?>
<!--==========================================================================================-->
<!--================================= Scripts ================================================-->
<!--==========================================================================================-->
<script src="Main001.js" type="text/javascript"></script>

<!------------------------------ Page transition & Class Change -------------------------------->
<!--==========================================================================================-->
<script type="text/javascript">


	$(document).ready(function() {
		$('.grid-nav li a').on('click', function(event){
			event.preventDefault();
			$('.grid-container').fadeOut(500, function(){
				$('#' + gridID).fadeIn(500);
			});
			var gridID = $(this).attr("data-id");

			$('.grid-nav li a').removeClass("active");
			$(this).addClass("active");
		});
	});
	$(function() {
		$('#b').hover(function() {
			$('#a').css('background-color', 'transparent');
			$('#a').css('transition', '1s');
			$('#a').css('color', '#000');
			$('#c , #d, #e, #f, #g, #h').css('transition', '1s');
			$('#c , #d, #e, #f, #g, #h').css('color', '#4e4e4e');
		}, function() {
			$('#a').css('background-color', '');
			$('#a').css('color', '');
			$('#c , #d, #e, #f, #g, #h').css('color', '');
		});
	});


	headerHeight = $("#b").height() + 5;

	$("#c").click(function() {
		$('html, body').animate({
			scrollTop: $("#homeTop").offset().top - headerHeight
		}, 750);
	});

	$("#d").click(function() {
		$('html, body').animate({
			scrollTop: $("#two").offset().top - headerHeight
		}, 750);
	});

	$("#e").click(function() {
		$('html, body').animate({
			scrollTop: $("#four").offset().top - headerHeight
		}, 750);
	});

	$("#f").click(function() {
		$('html, body').animate({
			scrollTop: $("#five").offset().top - headerHeight
		}, 750);
	});

	$("#g").click(function() {
		$('html, body').animate({
			scrollTop: $("#three").offset().top -headerHeight
		}, 750);
	});

	$("#totop").hide();
	$(window).scroll(function () {
		var scrollTop = (document.body).scrollTop;
		if ( scrollTop > 100 ) {
			$("#totop").stop().fadeTo(300, 1); 
		}
		else{
			$("#totop").stop().fadeTo(300, 0);
		}
	});

	$("#totop").click(function(){
		$('html, body').animate({scrollTop:0}, 500); return false;
	});
</script>


</body>
</html>