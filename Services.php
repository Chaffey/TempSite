<?php
/**
 * Created by PhpStorm.
 * User: Chaffey007
 * Date: 10/6/2016
 * Time: 10:24
 */
?>


<!--///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
										HTML
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////-->
<html>
<head>

    <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!--====================================Style Sheets================================-->
    <link rel="stylesheet" href="Main001.css"/>
    <link rel="stylesheet" type="text/css" href="Cagliostro-Regular.ttf" />

    <title>
        Products
    </title>
</head>
<body>

<div class="third" id="three">
    <center>
        <table width=100%>
            <tr>
                <td align=center width=70%>
                    <div class="container">
                        <font id="prod">Products</font>
                        </br>
                        <img src="Decorative-Line-Black-Free-Download-PNG.png" style="max-width:50%;"/>
                        <ul class="grid-nav">
                            <li><a href="#" data-id="two-columns" class="active">HAIRCARE</a></li>
                            <li><a href="#" data-id="three-columns">HAIRCOLOR</a></li>
                            <li><a href="#" data-id="four-columns">HAIRSTYLES</a></li>
                        </ul>

                        <hr />

                        <div id="two-columns" class="grid-container" style="display:block;">
                            <ul class="rig columns-2">
                                <li>
                                    <a href="#popup1"><img src="045.jpg" /></a>
                                    <h3>Styling</h3>
                                    <p>We can do all popular styles.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="046.jpg" /></a>
                                    <h3>Moisturiser</h3>
                                    <p>Moisturise your hair for that ultra shine.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="047.jpg" /></a>
                                    <h3>Conditioner</h3>
                                    <p>Take care of your hair with the best conditioning products to date.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="048.jpg" /></a>
                                    <h3>Wax</h3>
                                    <p>For that natural ragged look.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="049.jpg" /></a>
                                    <h3>Wax</h3>
                                    <p>For that natural ragged look.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="050.jpg" /></a>
                                    <h3>Wax</h3>
                                    <p>For that natural ragged look.</p>
                                </li>
                            </ul>
                        </div>
                        <!--/#two-columns-->

                        <div id="three-columns" class="grid-container">
                            <ul class="rig columns-3">
                                <li>
                                    <a href="#popup1"><img src="015.jpg" /></a>
                                    <h3>Precision Cut</h3>
                                    <p>All those extremely difficult styles.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="016.jpg" /></a>
                                    <h3>Specialist Tools</h3>
                                    <p>Tools used by professionals for specialising.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="017.jpg" /></a>
                                    <h3>Clippers</h3>
                                    <p>Easy to use, easy to clean. Save yourself a lot of time.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="018.jpg" /></a>
                                    <h3>Dryers</h3>
                                    <p>Industrial, everyday use and portable dryers.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="019.jpg" /></a>
                                    <h3>Scissor Sets</h3>
                                    <p>Get any and every type of scissor you need.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="020.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="021.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="022.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="023.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="024.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="025.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="026.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="027.jpg" /></a>
                                    <h3>Starter Kits</h3>
                                    <p>For those aspiring hair-stylists.</p>
                                </li>
                            </ul>
                        </div>
                        <!--/#three-columns-->

                        <div id="four-columns" class="grid-container">
                            <ul class="rig columns-4">
                                <li>
                                    <a href="#popup1"><img src="028.jpg" /></a>
                                    <h3>Styling kit</h3>
                                    <p>All the tools you need to style your hair in any way you can think of.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="029.jpg" /></a>
                                    <h3>Hair Spray</h3>
                                    <p>For those quick touch-ups and ultra strong hold.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="030.jpg" /></a>
                                    <h3>Shaving Cream</h3>
                                    <p>Feel like a real man with these assorted creams.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="031.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="032.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="033.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="034.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="035.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="036.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="037.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="038.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="039.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="040.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="041.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="042.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="043.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                                <li>
                                    <a href="#popup1"><img src="044.jpg" /></a>
                                    <h3>Hair Dye</h3>
                                    <p>Only the best coloring products for the perfect result.</p>
                                </li>
                            </ul>
                        </div>
                        <!--/#four-columns-->

                        <hr />

                    </div>
                    <!--/.container-->
                </td>
            </tr>
        </table>
    </center>
</div>
<!--================================= Forth ================================================-->
<div class="forth" id="four">
    <div class="about">
        <center>
            <table  class="tableAbout" align="center">
                <tr>
                    <td class="padsMin" width="100%" align="center">
                        <font id="medPlus">ABOUT US</font>
                    </td>
                </tr>
                <tr class="padsMin">
                    <td align="center">
                        <p><font id="med">ALPHA Q</font></p>
                        </br>
                        <font id="small">
                            <p>MEMBERS:</br></br>Fred</br>Altus</br>Edwin</br>Wynand</br>Stian</br>Stephan</br>Simeon</br></p>
                        </font>
                    </td>
                </tr>
            </table>
        </center>
    </div>
</div>

<!--================================= Fifth ================================================-->
<div class="fifth" id="five">
    <div class="about">
        <center>
            <table class="tableAbout" align="center">
                <tr>
                    <td class="padsMin" width="100%" align="center">
                        <font id="medPlus">FIND US</font>
                    </td>
                </tr>

                <tr>
                    <td>
                        <ul>
                            <li>
                                <font  id="small">
                                    70 Steve Biko Street Potchefstroom, 2520
                                </font>
                                </br></br>
                                <font id="small">
                                    </br>mhcpotchefstroom @gmail.com
                                </font>
                                <font id="small">
                                    </br></br>Tel: 123-456-7890
                                    </br></br></br></br>
                                    OPENING HOURS:
                                    </br>
                                    <p>
                                    <table width="100%" align="center">
                                        <tr>
                                            <td>
                                                <font id="small">Mon - Fri:</font>
                                            </td>
                                            <td>
                                                <font id="small">7am - 10pm</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <font id="small">Saturday:</font>
                                            </td>
                                            <td>
                                                <font id="small">8am - 10pm</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <font id="small">​Sunday:</font>
                                            </td>
                                            <td>
                                                <font id="small">8am - 11pm</font>
                                            </td>
                                        </tr>
                                    </table>
                                    </p>
                                </font>
                            </li>
                            <li>
                                </br>
                                <font id="small">
                                    Have a question or complaint?</br>Send us an email and we will contact you as soon as possible.
                                </font>
                                <form action="MailStatus.php" method="post" name="contactform">
                                    </br></br>
                                    <input class="stdIn" id="namefield" name="name" type="text" placeholder="Name" />
                                    </br></br>
                                    <input class="stdIn" id="emailfield" name="email" type="text" placeholder="Email" />
                                    </br></br>
                                    <input class="stdIn" id="subjectfield" name="subject" type="text" placeholder="Subject" />
                                    </br></br>
                                    <textarea class="stdIn" id="messagefield" name="message" cols="40" rows="5" placeholder="Message" ></textarea>
                                    </br></br>
                                    <input type="submit" class="sendBut" name="SendBut" value="Send" onclick="getInput()">
                                </form>
                            </li>
                        </ul>
                    </td>
                </tr>



            </table>
            </td>
            </tr>
            </table>
        </center>
    </div>
</div>
</div>
<!--==========================================================================================-->
<!--================================= Scripts ================================================-->
<!--==========================================================================================-->
<script src="Main001.js" type="text/javascript"></script>

<!------------------------------ Page transition & Class Change -------------------------------->
<!--==========================================================================================-->
<script type="text/javascript">


    $(document).ready(function() {
        $('.grid-nav li a').on('click', function(event){
            event.preventDefault();
            $('.grid-container').fadeOut(500, function(){
                $('#' + gridID).fadeIn(500);
            });
            var gridID = $(this).attr("data-id");

            $('.grid-nav li a').removeClass("active");
            $(this).addClass("active");
        });
    });

</script>
</body>
</html>